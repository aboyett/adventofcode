//package gitlab.com/aboyett/adventofcode/2019/day1
package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

// moduleFuel returns the fuel required for a module
func moduleFuel(mass int64) int64 {
	return int64(math.Floor(float64(mass)/3) - 2)
}

func main() {
	var total int64 = 0

	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Errorf("Failed to open file: %w", err)
		os.Exit(1)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		moduleMass, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			fmt.Errorf("Failed to parse line: %w", err)
		}
		fuel := moduleFuel(moduleMass)
		fmt.Printf("Input: %v Number: %v Output: %v\n", scanner.Text(), moduleMass, fuel)
		total += fuel
	}

	fmt.Printf("Total: %v\n", total)
}
