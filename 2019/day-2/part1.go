// IntComp machine
package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
)

// DeserCSVString calls DeserCSVReader against a string
func DeserCSVString(input string) ([]int, error) {
	return DeserCSVReader(strings.NewReader(input))
}

// DeserCSVReader decodes CSV format Intcode programs into an []int
func DeserCSVReader(input io.Reader) ([]int, error) {
	csvReader := csv.NewReader(input)

	record, err := csvReader.Read()

	if err == io.EOF {
		fmt.Printf("No records found")
		//break
	}
	if err != nil {
		log.Fatal(err)
	}

	intRecord := make([]int, len(record))

	for index, field := range record {
		value, err := strconv.Atoi(field)

		if err == strconv.ErrSyntax {
			fmt.Printf("Failed to decode \"%v\" into an int", value)
			return nil, err
		}

		intRecord[index] = value
	}

	return intRecord, nil
}

// InterpretIntCode will execute an IntCode program and return the end state
func InterpretIntCode(program []int) ([]int, error) {
	const InstrLength int = 4

	// pc, the programcounter, stores the index of the currently interpreted opcode
	for pc := 0; pc < len(program); pc += InstrLength {
		opcode := program[pc]

		switch opcode {
		case 1: // add
			registers := program[pc+1 : pc+InstrLength]
			program[registers[2]] = program[registers[0]] + program[registers[1]]
		case 2: // multiply
			registers := program[pc+1 : pc+InstrLength]
			program[registers[2]] = program[registers[0]] * program[registers[1]]
		case 99: // exit
			return program, nil
		default: // undefined opcode
			panic("Unknown opcode encountered: " + strconv.Itoa(opcode))
		}
	}

	return program, nil
}

func main() {
	// test programs
	//in := "1,0,0,0,99"
	//in := "2,3,0,3,99"
	//in := "2,4,4,5,99,0"
	//in := "1,1,1,4,99,5,6,0,99"

	// my test input
	in := "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,13,19,2,9,19,23,1,23,6,27,1,13,27,31,1,31,10,35,1,9,35,39,1,39,9,43,2,6,43,47,1,47,5,51,2,10,51,55,1,6,55,59,2,13,59,63,2,13,63,67,1,6,67,71,1,71,5,75,2,75,6,79,1,5,79,83,1,83,6,87,2,10,87,91,1,9,91,95,1,6,95,99,1,99,6,103,2,103,9,107,2,107,10,111,1,5,111,115,1,115,6,119,2,6,119,123,1,10,123,127,1,127,5,131,1,131,2,135,1,135,5,0,99,2,0,14,0"

	record, err := DeserCSVString(in)

	// set initial input state to the 1202 error
	record[1], record[2] = 12, 2

	if err == io.EOF {
		fmt.Printf("No records found")
		//break
	}
	if err != nil {
		log.Fatal(err)
	}

	InterpretIntCode(record)
	fmt.Printf("Output position 0 value: %v\n", record[0])
}
